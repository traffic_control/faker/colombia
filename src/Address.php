<?php

namespace Faker\Colombia;

use Faker\Extension\Extension;

class Address extends \Faker\Provider\es_ES\Address implements Extension
{
    protected static $buildingNumber = ['###', '##', '#'];
    protected static $streetPrefix = [
        'Calle', 'Avenida', 'Av.', 'Cl.', 'Carretera', 'Callejón', 'Vereda'
    ];
    protected static $streetSuffix = ['Norte', 'Este', ' Sur', ' Oeste'];

    protected static $postcode = ['%#####'];

    /**
     * @link https://en.wikipedia.org/wiki/Departments_of_Colombia
     */
    protected static $state = [
        'Capital District', 'Amazonas', 'Antioquia', 'Arauca', 'Atlántico', 'Bolívar', 'Boyacá', 'Caldas', 'Caquetá', 'Casanare', 'Cauca',
        'Cesar', 'Chocó', 'Córdoba', 'Cundinamarca', 'Guainía', 'Guaviare', 'Huila', 'La Guajira', 'Magdalena', 'Meta', 'Nariño',
        'Norte de Santander', 'Putumayo', 'Quindío', 'Risaralda', 'San Andrés y Providencia', 'Santander', 'Sucre', 'Tolima',
        'Valle del Cauca', 'Vaupés', 'Vichada'];

    /**
     * The list is not final
     */
    protected static $community = [
        'Arauca', 'Arauquita', 'Cravo Norte', 'Fortul', 'Puerto Rondón', 'Saravena', 'Tame', 'El Encanto', 'La Chorrera', 'La Pedrera',
        'La Victoria', 'Leticia', 'Mirití-Paraná', 'Puerto Alegría', 'Puerto Arica', 'Puerto Nariño', 'Puerto Santander', 'Tarapacá',
        'Baranoa', 'Barranquilla', 'Campo de la Cruz', 'Candelaria', 'Galapa', 'Juan de Acosta', 'Luruaco', 'Malambo', 'Manatí',
        'Palmar de Varela', 'Piojó', 'Polonuevo', 'Ponedera', 'Puerto Colombia', 'Repelón', 'Sabanagrande', 'Sabanalarga', 'Santa Lucía',
        'Santo Tomás', 'Soledad', 'Suán', 'Tubará', 'Usiacurí'
    ];

    protected static $cityFormats = [
        '{{cityPrefix}} {{firstName}}{{citySuffix}}',
        '{{cityPrefix}} {{firstName}}',
        '{{firstName}} {{citySuffix}}',
        '{{lastName}} {{citySuffix}}',
    ];
    protected static $streetNameFormats = [
        '{{streetPrefix}} {{firstName}}',
        '{{streetPrefix}} {{lastName}}',
        '{{streetPrefix}} {{firstName}} {{lastName}}'
    ];
    protected static $streetAddressFormats = [
        '{{streetName}}, {{buildingNumber}}, {{secondaryAddress}}',
        '{{streetName}}, {{secondaryAddress}}',
    ];
    protected static $addressFormats = [
        '{{streetAddress}}, {{city}} Edo. {{state}}',
        '{{streetAddress}}, {{city}} Edo. {{state}}, {{postcode}}'
    ];
}
