<?php

namespace Faker\Test\Colombia;

use Faker\Colombia\Address;
use Faker\Generator;
use PHPUnit\Framework\TestCase;

class AddressTest extends TestCase
{
    /**
     * @var Generator
     */
    private $_faker;

    public function setUp(): void
    {
        $faker = new Generator();
        $faker->seed(1);
        $faker->addProvider(new Address($faker));
        $this->_faker = $faker;
    }

    public function testPostCode()
    {
        $pattern = '/^\d{6}$/';

        $postCode = $this->_faker->postCode;
        $this->assertMatchesRegularExpression($pattern, $postCode);
    }

    public function testCommunity()
    {
        $community = $this->_faker->community;
        $this->assertSame(true, is_string($community) && $community !== '', 'Community name is not a valid string');
    }

    public function testState()
    {
        $state = $this->_faker->state;
        $this->assertSame(true, is_string($state) && $state !== '', 'State name is not a valid string');
    }
}
