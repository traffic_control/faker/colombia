<?php

namespace Faker\Test\Colombia;

use Faker\Colombia\Person;
use Faker\Generator;
use PHPUnit\Framework\TestCase;

class PersonTest extends TestCase
{
    /**
     * @var Generator
     */
    private $_faker;

    public function setUp(): void
    {
        $faker = new Generator();
        $faker->seed(1);
        $faker->addProvider(new Person($faker));
        $this->_faker = $faker;
    }

    public function testNuip()
    {
        $pattern = '/\d{10}/';

        $nuip = $this->_faker->nuip;
        $this->assertMatchesRegularExpression($pattern, $nuip);
    }
}
