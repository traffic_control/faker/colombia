<?php

namespace Faker\Test\Colombia;

use Faker\Colombia\PhoneNumber;
use Faker\Generator;
use PHPUnit\Framework\TestCase;

class PhoneNumberTest extends TestCase
{
    /**
     * @var Generator
     */
    private $_faker;

    public function setUp(): void
    {
        $faker = new Generator();
        $faker->seed(1);
        $faker->addProvider(new PhoneNumber($faker));
        $this->_faker = $faker;
    }

    public function testPhoneNumber()
    {
        $pattern = '/^((\+57|)(((300|301|302|303|304|305|31\d|320|321|322|323|324|350|351)\d{7})|([1-8][2-8]\d{6})))$/';

        $phoneNumber = str_replace(['-', ' '], [], $this->_faker->phoneNumber);
        $this->assertMatchesRegularExpression($pattern, $phoneNumber);
    }
}
